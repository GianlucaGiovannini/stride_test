import React, { FC } from "react";
import Container from "./Components/Container";

import "./App.css";

const App: FC = () => {
  return (
    <div className="App">
      <main id="main">
        <Container />
      </main>
    </div>
  );
};

export default App;
