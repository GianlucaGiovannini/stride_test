import React, { ChangeEvent } from "react";

// props types
interface IAdd {
  task: string;
  onCreateTaskList(event: ChangeEvent<HTMLTextAreaElement>): void;
  addTask(): void;
  showToggle(): void;
}

const AddElement = ({ task, onCreateTaskList, addTask, showToggle }: IAdd) => {
  return (
    <div className="add_container">
      <div className="add_area">
        <textarea
          name="task"
          placeholder="Inserisci voce"
          value={task}
          onChange={onCreateTaskList}
        ></textarea>
        <button className="btn_add" onClick={addTask}>
          Salva
        </button>
      </div>
      <div className="layer_add" onClick={showToggle}></div>
    </div>
  );
};

export default AddElement;
