import React, { useState } from "react";

// IMPORT PROPS TYPE INTERFACE
import { IList } from "../Interfaces/Interfaces";

// IMPORT DELLE IMMAGINI
import imgBin from "../img/Bin.svg";
import imgCheckbox_Off from "../img/Checkbox_Off.svg";
import imgCheckbox_On from "../img/Checkbox_On.svg";

// props types
interface Props {
  task: IList;
  updateTask(taskToUpdate: string, taskCheck: boolean): void;
  removeTask(taskToDelete: string): void;
}

const Task = ({ task, updateTask, removeTask }: Props) => {
  // useState per modificare l'immagine di checkbox
  const [imgCheckbox, setImgCheckbox] = useState<boolean>(task.checked);
  let toggleCheckBox = imgCheckbox ? imgCheckbox_On : imgCheckbox_Off;

  // useState per modificare il testo
  const [textState, setTextState] = useState<boolean>(task.checked);
  let toggleClassText = textState ? "done" : "";

  // useState per modificare il flex order
  const [order, setOrder] = useState<boolean>(task.checked);
  let toggleClassOrder = order ? "order" : "";

  /**
   * ### TOGGLE UPDATE
   * modifica tutti gli hooks del componente tramite il valore vero o falso del componente
   * -  toggle dell'immagine checkbox
   * - toggle della classe .done (per barrare il testo)
   * - toggle della classe .order (per ordinare le task)
   * - invio valore alla funzione updateTask per modificare la lista nel localStorage
   */
  const toggleUpdate = () => {
    setImgCheckbox(!task.checked);
    setTextState(!task.checked);
    setOrder(!task.checked);
    task.checked = !task.checked;
    updateTask(task.text, task.checked);
  };

  return (
    <div className={`task ${toggleClassOrder}`}>
      {/* "delete" */}
      <img
        className="bin"
        src={imgBin}
        alt="Bin immagine"
        onClick={() => {
          removeTask(task.text);
        }}
      />

      {/* update */}
      <img
        className="check"
        src={toggleCheckBox}
        alt="Checkbox"
        onClick={toggleUpdate}
      />

      {/* text */}
      <span className={`task_text ${toggleClassText}`}>{task.text}</span>
    </div>
  );
};

export default Task;
