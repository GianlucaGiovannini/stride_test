import React, { ChangeEvent, useEffect, useState } from "react";

// IMPORT PROPS TYPE INTERFACE
import { IList } from "../Interfaces/Interfaces";

// IMPORT COMPONENTS
import Task from "./Task";
import AddElement from "./AddTask";

// IMPORT DELLE IMMAGINI
import imgAdd from "../img/Adds.svg";

// LOCAL STORAGE per prendere i dati
const getLocalItem = () => {
  let currentValue;
  let getTask = localStorage.getItem("task");
  if (getTask) {
    currentValue = JSON.parse(localStorage.getItem("task") || String([]));
    return currentValue;
  } else {
    return [];
  }
};

const Container = () => {
  // useState per far apparire o sparire il componente AddElement
  const [showAdd, setShowAdd] = useState<boolean>(false);
  // useState per settare una nuova task
  const [task, setTask] = useState<string>("");
  // useState per settare la lista con le task
  const [list, setList] = useState<IList[]>(getLocalItem());

  /**
   * ### SHOWTOGGLE
   * - setta l'opposto del valore precedente alla variabile showAdd
   */
  const showToggle = () => {
    setShowAdd(!showAdd);
  };

  /**
   * ### SETTA UNA STRINGA PER LE NUOVE TASK
   *
   * - questa funzione serve a settare la task che l'utente digita
   * @param event  textArea event
   */
  const onCreateTaskList = (event: ChangeEvent<HTMLTextAreaElement>): void => {
    // setState(state);
    setTask(event.target.value);
  };

  /**
   * ### AGGIUNGI TASK
   * questa funzione serve ad aggiungere elementi alla lista delle task
   * - se il valore di task è vuoto non fa niente
   * - invoca la funzione showToggle per rimuovere l'elemento dalla dom
   * - crea un oggetto da inserire nella lista delle task
   * - pusha l'oggetto nell'array list
   * - setta  la text area a ""
   */
  const addTask = (): void => {
    const checkTask = new Set<string>(task);

    if (checkTask.size > 1) {
      showToggle();
      const newTask = {
        text: task,
        checked: false,
      };
      setList([newTask, ...list]);
      showToggle();
      setTask("");
    }
    setTask("");
  };

  /**
   * ### UPATETASK
   * questa funzione serve a modificare il local storage con i nuovi valori booleani per le task
   * @param taskToUpdate string
   * @param taskCheck boolean
   */
  const updateTask = (taskToUpdate: string, taskCheck: boolean) => {
    for (const element of list) {
      if (element.text === taskToUpdate) element.checked = taskCheck;
    }
    localStorage.setItem("task", JSON.stringify(list));
  };

  /**
   * ### REMOVE TASK
   * questa funzione serve ad aggiornare la lista quando viene rimossa una task
   * @param taskToDelete string
   */
  const removeTask = (taskToDelete: string): void => {
    setList(
      list.filter((task) => {
        return task.text !== taskToDelete;
      })
    );
  };

  // LOCAL STORAGE AGGIORNARE I DATI
  useEffect(() => {
    localStorage.setItem("task", JSON.stringify(list));
  }, [list]);

  return (
    <section id="surface">
      <div className="container">
        <div className="tasks_container">
          <h1>Titolo</h1>
          {list.map((task: IList) => {
            return (
              <Task
                key={task.text}
                task={task}
                updateTask={updateTask}
                removeTask={removeTask}
              />
            );
          })}
        </div>
      </div>

      <button className="btn_new_task" onClick={showToggle}>
        <img src={imgAdd} alt="Add button" />
        Nuova Voce
      </button>

      {/* corto circuito con l'operatore && per mostrare l'elemento AddElement */}
      {showAdd && (
        <AddElement
          task={task}
          onCreateTaskList={onCreateTaskList}
          addTask={addTask}
          showToggle={showToggle}
        />
      )}
    </section>
  );
};

export default Container;
